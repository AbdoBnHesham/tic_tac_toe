// Just for faster log
const log = console.log

// Take an name and mark and return Player object
function makePlayer(name, mark) {
    return {
        name: name,
        mark: mark,
        score: 0,
        getName: function () {
            return this.name.charAt(0).toUpperCase() + this.name.slice(1);
        },
        changeName: function (new_name) {
            this.name = new_name;
        },
        getScore: function () {
            return this.score.toString();
        },
        increaseScore: function () {
            this.score += 1;
        },
        resetScore: function () {
            this.score = 0;
        },
    }
}

// Init players object
let players = {
    p1: makePlayer("P1", "X"),
    p2: makePlayer("P2", "O"),
    p: null,
    played: 0,
    // Determine which player has the turn to play and choose randomly if it's first play, 
    // and if the board has filled return null
    nextPlayer: function () {
        if (this.played == 9) { return null }

        this.played += 1

        if (this.p === null) { this.p = this.randomPlayer() }
        else { this.p = this.p == this.p1 ? this.p2 : this.p1 }

        return this.p
    },
    // Return a random player
    randomPlayer: function () {
        return Math.floor(Math.random() * 10) <= 4 ? this.p1 : this.p2;
    },
    // Get the player by his mark only if it's "O" or "X"
    findPlayerByMark: function (mark) {
        if (mark === "X" | mark === "O") {
            if (this.p1.mark === mark) {
                return this.p1
            }
            return this.p2
        }
    },
    // Reset how much players has played to play again and set the next player
    resetPlayed: function () {
        this.played = 0
        this.nextPlayer()
    },
    // Reset the score of all players
    resetScore: function () {
        this.p1.resetScore()
        this.p2.resetScore()
    }
}

// Init the state
let state = {
    message_elem: document.getElementById("message"),
    score_elems: document.getElementById("score").children,
    play_again_button: document.getElementById("play-again"),
    reset_button: document.getElementById("reset"),

    // Update the message text with new message
    updateMessage: function (message) {
        this.message_elem.innerText = message
    },
    // Update the message to show draw message
    showDrawMessage: function () {
        this.updateMessage("Draw");
    },
    // Update the message to show the winner
    showWinMessage: function (winner) {
        this.updateMessage(winner + " has won");
    },
    // Update the message to show whose turn message
    updateTurnMessage: function () {
        this.updateMessage(players.p.getName() + " turn");
    },
    //TODO refactor
    // Update scores
    updateScores: function (players) {
        this.score_elems[1].innerText = players.p1.getScore();
        this.score_elems[3].innerText = players.p2.getScore();
    },
    //TODO refactor
    // Update players names on score section
    updatePlayersNames: function (players) {
        this.score_elems[0].innerText = players.p1.getName();
        this.score_elems[2].innerText = players.p2.getName();
    },
    // Show play again button
    showPAButton: function () {
        this.play_again_button.style.setProperty("visibility", "visible");
    },
    // Hide play again button
    hidePAButton: function () {
        this.play_again_button.style.setProperty("visibility", "hidden");
    },
};

// Take box element and return Box object
function makeBox(elem) {
    return {
        elem: elem,
        //TODO refactor
        // Add onclick event that add the mark, check the winner if any, and show draw message if it's draw
        addEvent: function () {
            this.elem.onclick = function () {
                if (this.firstElementChild.innerText === "") {
                    this.firstElementChild.innerText = players.p.mark

                    let winner_mark = board.checkForWinnerMark()
                    if (winner_mark !== null) {
                        let winner = players.findPlayerByMark(winner_mark)
                        winner.increaseScore()
                        state.updateScores(players)
                        state.showWinMessage(winner.getName())
                        state.showPAButton()
                        board.removeEvents()
                    } else {
                        let next_player = players.nextPlayer()
                        if (next_player !== null) {
                            state.updateTurnMessage()
                        } else {
                            state.showDrawMessage()
                            state.showPAButton()
                            board.removeEvents()
                        }
                    }
                }
            }
        },
        // Remove onclick event
        removeEvent: function () {
            this.elem.onclick = null
        },
        // Get the mark of the box
        getMark: function () {
            let mark = this.elem.firstElementChild.innerText;
            return mark ? mark : null;
        },
        // Reset box from any mark
        reset: function () {
            this.elem.firstElementChild.innerText = ""
        }
    }
}

// Init the board
let board = {
    elems: [],
    modified: 0,
    possibleMatches: [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ],
    // get box's mark
    boxMark: function (index) {
        return this.elems[index].getMark()
    },
    // Check three boxes match
    checkMatch: function (i1, i2, i3) {
        let i1m = this.boxMark(i1)
        let i2m = this.boxMark(i2)
        let i3m = this.boxMark(i3)
        if (i1m !== null && i1m === i2m && i2m === i3m) {
            return i1m;
        }
        return null;
    },
    // Check for a winner and return him
    checkForWinnerMark: function () {
        for (let m of this.possibleMatches) {
            let matched = this.checkMatch(...m)
            if (matched !== null) { return matched; }
        }
        return null
    },
    // Reset every box element to be empty of any mark
    reset: function () {
        for (let e of this.elems) {
            e.reset()
        }
    },
    // Add events to every box element
    addEvents: function () {
        for (let b of this.elems) {
            b.addEvent()
        }
    },
    // Remove events from every box element
    removeEvents: function () {
        for (let b of this.elems) {
            b.removeEvent()
        }
    }
};

// Get every box and add it to board object
for (let e of document.getElementById("board").children) {
    let box = makeBox(e)
    box.addEvent()
    board.elems.push(box)
}

// Reset the board to play again
function playAgain() {
    state.hidePAButton()
    board.reset()
    board.addEvents()
    players.resetPlayed()
    state.updateTurnMessage()
}
// Reset the board and scores
function reset() {
    playAgain()
    players.resetScore()
    state.updateScores(players)
}

state.play_again_button.onclick = playAgain
state.reset_button.onclick = reset


// start the game
players.nextPlayer()
state.updateTurnMessage()
state.updateScores(players)